-- All of these are minimal, just to test. The actual tables have a lot
-- more columns, foreign keys, etc.

CREATE TABLE ftp_users (
	ftp_user_id   INTEGER    NOT NULL PRIMARY KEY,
	dir           TEXT       NOT NULL
);

CREATE TABLE video_download_local_log (
	attempt_id        INTEGER    NOT NULL,
	vid_id            INTEGER    NOT NULL,
	user_id           INTEGER    NOT NULL,
	date_downloaded   TEXT       NOT NULL
);

CREATE TABLE video_download_attempt_log (
	attempt_id       INTEGER    NOT NULL PRIMARY KEY,
	vid_id           INTEGER    NOT NULL,
	user_id          INTEGER    NOT NULL,
	date_attempted   TEXT       NOT NULL
);

CREATE TABLE videos (
	vid_id               INTEGER   NOT NULL PRIMARY KEY,
	local_filename       TEXT      NULL,
	local_download_set   INTEGER   NULL
);

CREATE TABLE local_file_sets (
	file_set_id    INTEGER    NOT NULL PRIMARY KEY,
	local_server   INTEGER    NOT NULL,
	extra_path     TEXT       NOT NULL
);

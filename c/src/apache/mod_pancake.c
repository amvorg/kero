#include <ap_config.h>
#include <apr_pools.h>
#include <apr_strings.h>
#include <apr_thread_proc.h>
#include <httpd.h>
#include <http_config.h>
#include <http_core.h>
#include <http_log.h>
#include <http_protocol.h>
#include <http_request.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <errno.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>

#ifdef APLOG_USE_MODULE
APLOG_USE_MODULE(pancake);
#endif


/*
 * Because no header seems to define this, bastards...
*/
#ifndef UNIX_PATH_LENGTH
#	define UNIX_PATH_LENGTH 108
#endif

/*
 * The default URI path prefix for mod_pancake.
*/
#define PANCAKE_DEFAULT_URI_PREFIX "/pancake/"
#define PANCAKE_DEFAULT_CLIENT_PREFIX "/var/run/apache2/mod_pancake/"
#define PANCAKE_DEFAULT_SERVER_SOCKET "/var/run/pancake/pancake.sock"

/*
 * The URI prefix suffix for various things. These aren't configurable
 * except at compile time. Consider them part of the mod_pancake API.
*/
#define PANCAKE_URI_SUFFIX_DL "dl/"
#define PANCAKE_URI_SUFFIX_STAT "stat/"
#define PANCAKE_URI_SUFFIX_TEST "test/"

/*
 * Request type. Found by common_token_handler.
*/
#define PANCAKE_REQUEST_DL    1  /* download video */
#define PANCAKE_REQUEST_TEST  2  /* test download before confirming */

/*
 * Some sanity values. First up, the maximum message size for the
 * socket. We'll either ignore or truncate messages longer than this.
 * Its also the size used for the receive buffer minus 1 byte (to allow
 * for null termination, of course).
*/
#define MAXIMUM_DATAGRAM_LENGTH 1024

/*
 * Some strings used in the protocol. Defined here to let the compiler
 * catch typos. Note the spaces after them; those are important. For
 * one, it will catch if there is ever an OKRA response code
 * implemented (why? Let's just ignore that). The other thing is that
 * otherwise you'll get a weird 404 error from looking for a filename
 * starting with a space.
*/
#define PROTOCOL_RESULT_OK "OK "
#define PROTOCOL_RESULT_ERROR "ERROR "

/*
 * Configuration for mod_pancake. Note that this structure will be
 * created as all-zeros; that should be interpreted as "default" for
 * everything.
*/
struct pancake_config {
	const char *uri_prefix_dl;
	const char *uri_prefix_stat;
	const char *uri_prefix_test;
	const char *client_prefix;
	enum {
		PANCAKE_ENABLED_UNKNOWN = 0,
		PANCAKE_ENABLED_NO,
		PANCAKE_ENABLED_YES
	} enabled;
	struct sockaddr_un server;
};
typedef struct pancake_config *pancake_config_ptr;


/*
 * Store the data needed for a socket: The file descriptor, and the
 * name. We need the name to unlink the file when we're done.
*/
struct socket_data {
	char *name;
	int fd;
	unsigned int msg_num;
};
typedef struct socket_data *socket_data_ptr;


// Stuff for the Apache module API to use.
static void *pancake_create_server_config(apr_pool_t *pool, server_rec *s);
static void *pancake_merge_server_config(apr_pool_t *pool, void *base_vp,
                                         void *new_vp);
static int pancake_post_config(apr_pool_t *pconf, apr_pool_t *plog,
                               apr_pool_t *ptemp, server_rec *s);
static void pancake_child_init(apr_pool_t *pchild, server_rec *s);
static int pancake_translator(request_rec *r);
static int pancake_handler(request_rec *r);
static int pancake_logger(request_rec *r);
static void pancake_register_hooks(apr_pool_t *p);

// Config handling functions
static const char *pancake_set_enable(cmd_parms *parms, void *, int flag);
static const char *pancake_set_socket(cmd_parms *parms, void *,
                               const char *sockname);
static const char *pancake_set_uri_prefix(cmd_parms *parms, void *,
                                          const char *prefix);
static const char *pancake_set_client_prefix(cmd_parms *parms, void *,
                                             const char *prefix);

// Functions used internally.
static void socket_threadkey_dtor(void *data_void);
static apr_threadkey_t *socket_key(apr_threadkey_t *newKey);
static apr_threadkey_t *get_socket_key();
static apr_threadkey_t *set_socket_key(apr_threadkey_t *newKey);
static pancake_config_ptr get_config(server_rec *s);
static socket_data_ptr get_thread_socket(request_rec *, pancake_config_ptr);
static int common_token_handler(request_rec *r, pancake_config_ptr *conf,
                                socket_data_ptr *sock, char **token,
                                int *token_request_type);
static int common_ftp_user_handler(request_rec *r, pancake_config_ptr *conf,
                                   socket_data_ptr *sock, long *id);
static bool send_to_pancake(request_rec *r, pancake_config_ptr conf,
                            socket_data_ptr sock, char *msg);
static int get_message_from_pancake(request_rec *r, const socket_data_ptr sock,
                                    char **message);
static bool range_contains_eof(request_rec *r);


/*
 * Return a blank new server configuration.
*/
static void *pancake_create_server_config(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct pancake_config));
}


/*
 * Merge two server configs.
*/
static void *pancake_merge_server_config(apr_pool_t *pool, void *base_vp,
                                         void *new_vp) {
	pancake_config_ptr base = (pancake_config_ptr)base_vp,
	                   new = (pancake_config_ptr)new,
	                   merge = apr_pcalloc(pool, sizeof(struct pancake_config));
	
	merge->uri_prefix_dl
		= (new->uri_prefix_dl) ? new->uri_prefix_dl : base->uri_prefix_dl;
	merge->uri_prefix_stat 
		= (new->uri_prefix_stat) ? new->uri_prefix_stat : base->uri_prefix_stat;
	merge->uri_prefix_test 
		= (new->uri_prefix_test) ? new->uri_prefix_test : base->uri_prefix_test;
	merge->client_prefix = (new->client_prefix) ?
		new->client_prefix : base->client_prefix;
	merge->enabled = (new->enabled) ? new->enabled : base->enabled;
	
	if (new->server.sun_path[0]) {
		memcpy(merge->server.sun_path, new->server.sun_path,
		       UNIX_PATH_LENGTH);
	} else {
		memcpy(merge->server.sun_path, base->server.sun_path,
		       UNIX_PATH_LENGTH);
	}

	return merge;
}


/*
 * Post config - check that the config is sane and fill in defaults for
 * missing values.
*/
static int pancake_post_config(apr_pool_t *pconf, apr_pool_t *plog,
                               apr_pool_t *ptemp, server_rec *s) {
	pancake_config_ptr conf = get_config(s);
	if (!conf)
		return APR_EGENERAL;

	/* Check if pancake is enabled. In the process, convert DEFAULT to
	 * NO (pancake defaults to off)
	*/
	if (conf->enabled == PANCAKE_ENABLED_UNKNOWN)
		conf->enabled = PANCAKE_ENABLED_NO;
	if (conf->enabled == PANCAKE_ENABLED_NO)
		return DECLINED;

	if (!conf->uri_prefix_dl || !conf->uri_prefix_stat
	    || !conf->uri_prefix_test) {
		ap_log_error(APLOG_MARK, APLOG_NOTICE, 0, s,
		             "Using default URI prefix of %s",
		             PANCAKE_DEFAULT_URI_PREFIX);
		conf->uri_prefix_dl
			= PANCAKE_DEFAULT_URI_PREFIX PANCAKE_URI_SUFFIX_DL;
		conf->uri_prefix_stat 
			= PANCAKE_DEFAULT_URI_PREFIX PANCAKE_URI_SUFFIX_STAT;
		conf->uri_prefix_test
			= PANCAKE_DEFAULT_URI_PREFIX PANCAKE_URI_SUFFIX_TEST;
	}

	if (!conf->client_prefix) {
		ap_log_error(APLOG_MARK, APLOG_NOTICE, 0, s,
		             "Using default client socket prefix %s.",
		             PANCAKE_DEFAULT_CLIENT_PREFIX);
		conf->client_prefix = PANCAKE_DEFAULT_CLIENT_PREFIX;
	}
	
	if (!conf->server.sun_path[0]) {
		ap_log_error(APLOG_MARK, APLOG_NOTICE, 0, s,
		             "Using default server socket %s.",
		             PANCAKE_DEFAULT_SERVER_SOCKET);
		conf->server.sun_family = AF_UNIX;
		strncpy(conf->server.sun_path, PANCAKE_DEFAULT_SERVER_SOCKET,
		        UNIX_PATH_LENGTH-1); // -1 == guarantee null terminator
	}

	return OK;
}


/*
 * Get this process ready for mod_pancake. In particular, this sets up
 * the thread-specific socket storage.
*/
static void pancake_child_init(apr_pool_t *pchild, server_rec *s) {
	apr_threadkey_t *key;
	apr_status_t r =
		apr_threadkey_private_create(&key, socket_threadkey_dtor, pchild);
	if (APR_SUCCESS != r) {
		ap_log_error(APLOG_MARK, APLOG_EMERG, r, s,
		             "Failed to create thread-private key.");
	} else
		set_socket_key(key);
}


/*
 * Talk to the Pancake server to translate a file name.
*/
static int pancake_translator(request_rec *r) {
	pancake_config_ptr conf;
	socket_data_ptr sock;
	char *token;
	int res, request_type;

	if (OK != (res = common_token_handler(r, &conf, &sock, &token,
	                                      &request_type)))
		return res;

	// Send it off to Pancake.
	char *msg;
	switch (request_type) {
		case PANCAKE_REQUEST_DL:
			msg = apr_psprintf(r->pool, "%i START %s", ++sock->msg_num, token);
			break;
		case PANCAKE_REQUEST_TEST:
			msg = apr_psprintf(r->pool, "%i TEST %s", ++sock->msg_num, token);
			break;
		default:
			ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r,
			              "Unknown request_type %i", request_type);
			return HTTP_INTERNAL_SERVER_ERROR;
	}

	if (!send_to_pancake(r, conf, sock, msg))
		return HTTP_INTERNAL_SERVER_ERROR;

	// Get the response.
	char *msg_start;
	if (OK != (res = get_message_from_pancake(r, sock, &msg_start)))
		return res;
	
	// We have a response from Pancake to work with. Parse and handle
	// it. First question, is it an error or a success? 
	if (0 == strncmp(msg_start, PROTOCOL_RESULT_ERROR,
	                 strlen(PROTOCOL_RESULT_ERROR))) {
		// This is an error message. The format of an error message is
		// an HTTP error number (e.g., 500) followed by a human-readable
		// message of what went wrong.
		int code;
		char *error_code, *error_msg;
		error_code = msg_start+strlen(PROTOCOL_RESULT_ERROR);
		code = (int)strtol(error_code, &error_msg, 10);
		if (code == 0 || (error_code == msg_start)) {
			ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
			              "Could not extract error from %s", msg_start);
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		ap_custom_response(r, code, error_msg);
		return code;
	}
	if (0 == strncmp(msg_start, PROTOCOL_RESULT_OK,
	                        strlen(PROTOCOL_RESULT_OK))) {
		// This is a success! Yippeee! Format is just the file name.
		r->filename = msg_start + strlen(PROTOCOL_RESULT_OK) + 1;
		apr_table_setn(r->headers_out, "Content-Disposition", "attachment");

		return OK;
	}
	
	// Damn. We failed both the check for an error and the check for a
	// success. Who knows what this is. Log an error, and spew an
	// internal server error to the user.
	ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r,
				  "Message does not make sense: %s", msg_start);
	return HTTP_INTERNAL_SERVER_ERROR;
}

static int pancake_handler(request_rec *r) {
	pancake_config_ptr conf;
	socket_data_ptr sock;
	long ftp_user_id;
	int res;

	if (OK != (res = common_ftp_user_handler(r, &conf, &sock, &ftp_user_id)))
		return res;
	if (r->header_only || r->method_number != M_GET)
		return HTTP_METHOD_NOT_ALLOWED;

	char *msg = apr_psprintf(r->pool, "%i STATUS %li", ++sock->msg_num,
	                         ftp_user_id);
	if (!send_to_pancake(r, conf, sock, msg))
		return HTTP_INTERNAL_SERVER_ERROR;

	char *reply;
	if (OK != (res = get_message_from_pancake(r, sock, &reply)))
		return res;

	ap_set_content_type(r, "text/plain");
	ap_set_content_length(r, strlen(reply));
	ap_rputs(reply, r);

	return OK;
}


/*
 * If the file was fully transferred, tell Pancake to log the transfer
 * completion. Otherwise, just do nothing. Fun logger, huh?
*/
static int pancake_logger(request_rec *r) {
	// before we do any computing, check if it even completed. If not,
	// just return.
	if (r->connection->aborted)
		return DECLINED;

	// We need to ignore error messages, otherwise those get logged as
	// completed downloads. 
	if (r->status != HTTP_OK && r->status != HTTP_PARTIAL_CONTENT)
		return DECLINED;

	// OK, we might have something to tell pancake. Go ahead and
	// proceed.
	pancake_config_ptr conf;
	socket_data_ptr sock;
	char *token;
	int res, request_type;

	if (OK != (res = common_token_handler(r, &conf, &sock, &token,
	                                      &request_type)))
		return res; // guess we didn't really have anything.

	if (request_type != PANCAKE_REQUEST_DL)
		return DECLINED; // only REQUEST_DL logs to db.
	
	// Well, its for us. Now, if its partial content, let's determine if
	// the last byte of the file was served. If it wasn't, we don't log
	// the download completed.
	if (HTTP_PARTIAL_CONTENT == r->status && !range_contains_eof(r))
		return DECLINED;

	// Tell Pancake.
	char *msg = apr_psprintf(r->pool, "%i FINISH %s", ++sock->msg_num, token);
	if (!send_to_pancake(r, conf, sock, msg))
		return HTTP_INTERNAL_SERVER_ERROR;

	// Get the response.
	char *msg_start;
	if (OK != (res = get_message_from_pancake(r, sock, &msg_start))) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, res, r,
		              "Guess we won't be completing %s", token);
		return res;
	}

	// Well, let's go ahead and parse the response just because. Not
	// like there is much we can do if it failed... Well, we can log to
	// the error log, but that's it.
	if (0 == strncmp(msg_start, PROTOCOL_RESULT_ERROR,
	                 strlen(PROTOCOL_RESULT_ERROR))) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
		              "Logging failed for %s: %s", token, msg_start);
		return DECLINED;
	}
	if (0 == strncmp(msg_start, PROTOCOL_RESULT_OK,
	                        strlen(PROTOCOL_RESULT_OK))) {
		return DECLINED; // I think we need to decline to let other handlers
	}

	ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
				  "Silly response for log on %s: %s", token, msg_start);
	return DECLINED;
}


static void pancake_register_hooks(apr_pool_t *p) {
	ap_hook_child_init(pancake_child_init, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_post_config(pancake_post_config, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_translate_name(pancake_translator, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_handler(pancake_handler, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_log_transaction(pancake_logger, NULL, NULL, APR_HOOK_MIDDLE);
}


/*
 * Set the enabled flag.
*/
static const char *pancake_set_enable(cmd_parms *parms, void *ignore,
                                      int flag) {
	pancake_config_ptr conf = get_config(parms->server);
	if (conf) {
		if (flag)
			conf->enabled = PANCAKE_ENABLED_YES;
		else
			conf->enabled = PANCAKE_ENABLED_NO;
		return NULL;
	} else
		return "Could not get config.";
}


/*
 * Set the socket name. We go ahead and build the sockaddr_un here.
*/
static const char *pancake_set_socket(cmd_parms *parms, void *ignore,
                               const char *sockname) {
	pancake_config_ptr conf = get_config(parms->server);
	if (!conf)
		return "Could not get config.";

	int len = strlen(sockname);
	if (len >= UNIX_PATH_LENGTH)
		return "Socket name >= UNIX_PATH_LENGTH";
	
	conf->server.sun_family = AF_UNIX;
	memcpy(conf->server.sun_path, sockname, len+1);
	return NULL;
}


static const char *pancake_set_uri_prefix(cmd_parms *parms, void *ignore,
                                          const char *prefix) {
	pancake_config_ptr conf = get_config(parms->server);
	if (!conf)
		return "Could not get config.";

	conf->uri_prefix_dl
		= apr_pstrcat(parms->pool, prefix, PANCAKE_URI_SUFFIX_DL, NULL);
	conf->uri_prefix_stat
		= apr_pstrcat(parms->pool, prefix, PANCAKE_URI_SUFFIX_STAT, NULL);
	conf->uri_prefix_test
		= apr_pstrcat(parms->pool, prefix, PANCAKE_URI_SUFFIX_TEST, NULL);
	return NULL;
}


static const char *pancake_set_client_prefix(cmd_parms *parms, void *ignore,
                                             const char *prefix) {
	pancake_config_ptr conf = get_config(parms->server);
	if (!conf)
		return "Could not get config.";

	conf->client_prefix = prefix;
	return NULL;
}


static const command_rec pancake_commands[] = {
	{ "Pancake", { .flag = pancake_set_enable }, NULL, RSRC_CONF, FLAG, "Pancake must be either On of Off" },
	{ "PancakeServerSocket", { .take1 = pancake_set_socket }, NULL, RSRC_CONF, TAKE1, "PancakeServerSocket must be followed by a file name (example: "PANCAKE_DEFAULT_SERVER_SOCKET")" },
	{ "PancakeURIPrefix", { .take1 = pancake_set_uri_prefix } , NULL, RSRC_CONF, TAKE1, "PancakeURIPrefix must be followed by a URI prefix (example: "PANCAKE_DEFAULT_URI_PREFIX")" },
	{ "PancakeClientPrefix", { .take1 = pancake_set_client_prefix }, NULL, RSRC_CONF, TAKE1, "PancakeClientPrefix must be followed by a file name prefix (example: "PANCAKE_DEFAULT_CLIENT_PREFIX")" },
	{ NULL }
};


module AP_MODULE_DECLARE_DATA pancake_module = {
	STANDARD20_MODULE_STUFF, 
	NULL,                  /* create per-dir    config structures */
	NULL,                  /* merge  per-dir    config structures */
	pancake_create_server_config,/* create per-server config structures */
	pancake_merge_server_config, /* merge  per-server config structures */
	pancake_commands,            /* table of config file commands       */
	pancake_register_hooks  /* register hooks                      */
};


static pancake_config_ptr get_config(server_rec *s) {
	pancake_config_ptr conf =
		ap_get_module_config(s->module_config, &pancake_module);
	if (!conf) {
		ap_log_error(APLOG_MARK, APLOG_CRIT, 0, s,
		             "Could not find module config in server!");
	}
	return conf;
}


/* Dispose of one of the sockets used to communicate with the Pancake
 * server. Basically, thise means close and unlink. We don't need to
 * clean up the memory used by the data structure itself as it is part
 * of an apr pool.
*/
static void socket_threadkey_dtor(void *data_void) {
	socket_data_ptr data = (socket_data_ptr)data_void;
	if (NULL == data)
		return;        // just in case

	close(data->fd);
	unlink(data->name);
}


/*
 * Keep track of the key to get the thread-private socket_data. If you
 * pass NULL for newKey, return the current key. If you pass non-null,
 * set the key to newKey and then return the /old/ value.
 *
 * This is really only for use by the get_socket_key and set_socket_key
 * functions (below); use them instead.
*/
static apr_threadkey_t *socket_key(apr_threadkey_t *newKey) {
	static apr_threadkey_t *key = NULL;
	apr_threadkey_t *savekey;
	if (newKey) {
		savekey = key;
		key = newKey;
		return savekey;
	} else
		return key;
}


/*
 * Get the current key to the thread-specific socket storage.
*/
static apr_threadkey_t *get_socket_key() {
	return socket_key(NULL);
}


/*
 * Set the key to the thread-specific socket storage. Return the old
 * value (or NULL if none).
*/
static apr_threadkey_t *set_socket_key(apr_threadkey_t *newKey) {
	return socket_key(newKey);
}


/*
 * Grab the socket for this thread from the thread-local data. If there
 * isn't one yet, go ahead and create it.
*/
static socket_data_ptr get_thread_socket(request_rec *r,
                                         pancake_config_ptr conf) {
	// Get our thread-private data key.
	apr_threadkey_t *key = get_socket_key();
	if (!key) {
		ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r,
		              "Couldn't get socket key.");
		return NULL;
	}

	// Grab the socket_data structure for this thread.
	void *data_ptr;
	apr_status_t res;
	if ((res = apr_threadkey_private_get(&data_ptr, key))) {
		ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r,
		              "Couldn't fetch socket_data from key");
		return NULL;
	}

	// If we already have a socket for this thread, use it.
	socket_data_ptr sock = (socket_data_ptr)data_ptr;
	if (sock)
		return sock;

	/*
	 * Well, it appears the the thread socket has not been created yet.
	 * So let's go ahead and make one. This takes several steps, and if
	 * they all succeed, we'll go ahead and save the socket for future
	 * use.
	*/
	// First: Create a socket.
	sock = apr_palloc(r->server->process->pool,
	                  sizeof(struct socket_data));
	if (!sock) {
		ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, 
		              "Couldn't create socket_data.");
		return NULL;
	}
	sock->msg_num = 0;
	if (-1 == (sock->fd = socket(PF_UNIX, SOCK_DGRAM, 0))) {
		ap_log_rerror(APLOG_MARK, APLOG_CRIT,
		              APR_FROM_OS_ERROR(errno), r, "Couldn't create socket.");
		return NULL;
	}

	// Second: Bind it to a unique filename starting with client_prefix.
	// We do the file name generation and bind in one step so that we
	// can try again if we hit a file-name conflict.
	unsigned int random_seed = time(NULL);
	do {
		struct sockaddr_un testaddr;
		testaddr.sun_family = AF_UNIX;
		snprintf(testaddr.sun_path, UNIX_PATH_LENGTH, "%s%i",
		         conf->client_prefix, rand_r(&random_seed));
		if (-1 == bind(sock->fd, (struct sockaddr *)&testaddr,
		               sizeof(testaddr))) {
			if (errno == EADDRINUSE)
				continue; // File already existed, try again.
			ap_log_rerror(APLOG_MARK, APLOG_CRIT,
			              APR_FROM_OS_ERROR(errno), r,
			              "Couldn't bind socket to %s", testaddr.sun_path);
			close(sock->fd);
			return NULL;
		}
		sock->name = apr_pstrdup(r->server->process->pool, testaddr.sun_path);
	} while (!sock->name);
	chmod(sock->name, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);

	// Third: Set it to non-blocking mode. We should never really need
	// this, but the manpage under linux warns that the kernel may lie
	// in results for select. See the "BUGS" section of select(2). Also,
	// we don't want to block if for some reason Pancake hangs and stops
	// reading its socket (could use MSG_DONTWAIT, but that's more
	// error-prone --- far too easy to forget one)
	int socket_flags;
	if (-1 == (socket_flags = fcntl(sock->fd, F_GETFL))) {
		ap_log_rerror(APLOG_MARK, APLOG_CRIT,
		              APR_FROM_OS_ERROR(errno), r,
		              "F_GETFL failed. Will leave in blocking mode!");
	} else {
		socket_flags |= O_NONBLOCK;
		if (-1 == fcntl(sock->fd, F_SETFL, socket_flags)) {
			ap_log_rerror(APLOG_MARK, APLOG_CRIT,
			              APR_FROM_OS_ERROR(errno), r,
			              "F_SETFL failed. Socket left in blocking mode!");
		}
	}
	
	// Finally, save it to the key.
	if ((res = apr_threadkey_private_set(sock, key))) {
		ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r,
		              "Couldn't save socket_data to key.");
		close(sock->fd); // don't leak the file descriptor.
		return NULL;
	}

	// We're done, return the socket.
	return sock;
}

/*
 * Do very basic stuff common to both the translator and logger hooks
 * (the ones that use a download token). This basic stuff includes
 * retreiving our module's config, our socket, and the download token
 * from the URI.
 *
 * OK will be returned if all went well. DECLINED will be returned if
 * this URI is not for us. Otherwise, an error (intended to be returned
 * by the translator hook) will be returned.
 *
 * If the result is OK, then all variables have been set. If it is
 * DECLINED, then only conf has been. Otherwise, all bets are off.
*/
static int common_token_handler(request_rec *r, pancake_config_ptr *conf,
                                socket_data_ptr *sock, char **token,
                                int *request_type) {
	*conf = get_config(r->server);
	if (!conf)
		return HTTP_INTERNAL_SERVER_ERROR;

	// Is this for us?
	char *token_start;
	if (0 == strncmp(conf[0]->uri_prefix_dl, r->uri,
	                 strlen(conf[0]->uri_prefix_dl))) {
	    *request_type = PANCAKE_REQUEST_DL;
		token_start = r->uri + strlen(conf[0]->uri_prefix_dl);
	} else if (0 == strncmp(conf[0]->uri_prefix_test, r->uri,
	                        strlen(conf[0]->uri_prefix_test))) {
	    *request_type = PANCAKE_REQUEST_TEST;
		token_start = r->uri + strlen(conf[0]->uri_prefix_test);
	} else
		return DECLINED;

	// It is, so go ahead and get our socket.
	*sock = get_thread_socket(r, *conf);
	if (sock == NULL)
		return HTTP_INTERNAL_SERVER_ERROR;

	// Find the download token.
	char *token_end = index(token_start, '/');
	if (!token_end) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
		              "Invalid pancake URI: %s", r->uri);
		return HTTP_BAD_REQUEST;
	}
	*token = apr_palloc(r->pool, token_end-token_start+1);
	memcpy(*token, token_start, token_end-token_start);
	token[0][token_end-token_start] = 0;

	return OK;
}


/*
 * Do very basic stuff common to hooks that take a ftp_user_id, not a
 * token. This basic stuff includes retreiving our module's config, our
 * socket, and the ftp_user_id from the URI.
 *
 * OK will be returned if all went well. DECLINED will be returned if
 * this URI is not for us. Otherwise, an HTTP error code will be
 * returned.
 *
 * If the result is OK, then all variables have been set. If it is
 * DECLINED, then only conf has been. Otherwise, all bets are off.
*/
static int common_ftp_user_handler(request_rec *r, pancake_config_ptr *conf,
                                   socket_data_ptr *sock, long *id) {
	*conf = get_config(r->server);
	if (!conf)
		return HTTP_INTERNAL_SERVER_ERROR;

	// Is this for us?
	if (strncmp(conf[0]->uri_prefix_stat, r->uri,
	            strlen(conf[0]->uri_prefix_stat)))
		return DECLINED;

	// It is, so go ahead and get our socket.
	*sock = get_thread_socket(r, *conf);
	if (*sock == NULL)
		return HTTP_INTERNAL_SERVER_ERROR;

	// Find the download token.
	char *start = r->uri + strlen(conf[0]->uri_prefix_stat);
	char *end;
	*id = strtol(start, &end, 10);
	if (end != start + strlen(start)) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
		              "Invalid pancake URI: %s", r->uri);
		return HTTP_BAD_REQUEST;
	}

	return OK;
}


/*
 * Send a message to Pancake. The message is expected to be a normal C
 * string, though the trailing NULL is not sent to Pancake.
 *
 * Returnes true if the message was sent. Returns false and logs a
 * warning if not.
*/
static bool send_to_pancake(request_rec *r, pancake_config_ptr conf,
                            socket_data_ptr sock, char *msg) {
	if (-1 == sendto(sock->fd, msg, strlen(msg), MSG_DONTWAIT,
	                 (struct sockaddr *)&conf->server, sizeof(conf->server))) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING,
		              APR_FROM_OS_ERROR(errno) , r, "send failed.");
		return false;
	}
	return true;
}


/*
 * Look for the message from Pancake in the socket given by <sock>. The
 * ID is checked against msg_num in <sock> and essages with other IDs are
 * discarded after logging a warning. The actual memory to store the
 * message is allocated by this function (off the request pool). In
 * other words, <message> is an output-only parameter.
 *
 * Return OK if the message is found. If anything else is returned, the
 * message could not be found and *<message> has not been assigned to. Values
 * returned are intended to be returned by the translator hook.
*/
static int get_message_from_pancake(request_rec *r, const socket_data_ptr sock,
                                    char **message) {
	// Use a select() w/ a timeout so we can error out of Pancake
	// doesn't respond. Better to give the user an error message than
	// just hang... Allocate a buffer one bigger than the receive length
	// so we can null-terminate.
	char *recv_buffer = apr_palloc(r->pool, MAXIMUM_DATAGRAM_LENGTH+1);
	ssize_t recv_amount = 0;
	while (1) {
		// use select to wait until the socket is readable.
		fd_set readfds;
		int nfds;
		struct timeval wait_time = { 5, 0 };
		FD_ZERO(&readfds);
		FD_SET(sock->fd, &readfds);
		if (-1 == (nfds=select(sock->fd+1,&readfds,NULL,NULL,&wait_time))) {
			// select failed for some reason. This shouldn't happen.
			ap_log_rerror(APLOG_MARK, APLOG_ERR,
			              APR_FROM_OS_ERROR(errno), r, "select failed.");
			return HTTP_INTERNAL_SERVER_ERROR;
		} else if ( 0 == nfds) {
			// select timeed out. This might happen if pancake is far
			// too busy. Log a warning.
			ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
			              "Pancake did not respond to request.");
			return HTTP_GATEWAY_TIME_OUT;
		}

		// select says we can read. go ahead and try.
		if (-1 == (recv_amount = recv(sock->fd, recv_buffer,
									  MAXIMUM_DATAGRAM_LENGTH,
									  MSG_TRUNC))) {
			ap_log_rerror(APLOG_MARK, APLOG_WARNING,
						  APR_FROM_OS_ERROR(errno), r, "recv failed");
			return HTTP_INTERNAL_SERVER_ERROR;
		} 

		// We did read. Null terminate the message.
		if (recv_amount > MAXIMUM_DATAGRAM_LENGTH) {
			// The message was too long and got truncated. Log a
			// warning, then set the received amount to the max
			// (so we don't overflow the buffer below)
			ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
						  "Truncated oversized message, len=%li",
						  (long)recv_amount);
			recv_amount = MAXIMUM_DATAGRAM_LENGTH;
		}
		recv_buffer[recv_amount] = 0; // null terminate

		// Start parsing. First step is to pull out the message number.
		unsigned int got_msg_num = 0;
		int sscanf_found, message_offset;
		if (-1 == (sscanf_found = sscanf(recv_buffer, "%u %n",
		                                 &got_msg_num, &message_offset))) {
			// could not parse --- sscanf returned failure.
			ap_log_rerror(APLOG_MARK, APLOG_NOTICE,
			              APR_FROM_OS_ERROR(errno), r,
			              "sscanf failed, ignoring message: %s", recv_buffer);
			continue;
		} 
		if (0 == sscanf_found) {
			// could not parse, did not match format.
			ap_log_rerror(APLOG_MARK, APLOG_NOTICE, 0, r,
			              "corrupted message, ignoring: %s", recv_buffer);
			continue;
		}
		if (got_msg_num != sock->msg_num) {
			// did parse, but this is an old message.
			ap_log_rerror(APLOG_MARK, APLOG_NOTICE, 0, r,
			              "Old message: got=%u, wanted=%u. Ignoring.",
			              got_msg_num, sock->msg_num);
			continue;
		}
		
		// We got the right message number. Calculate where the real
		// message starts, then break out of the loop.
		*message = recv_buffer + message_offset;
		return OK;
	}
}


/*
 * Check if the Range: header contains a range which includes the last
 * byte in the file.
 *
 * Return true if it does; return false if not. If there is an error
 * (illegal range, no range, range not in bytes) false is also returned.
 * Note from that list that although the last two errors will likely
 * cause the last byte to be sent, this function still returns false.
 * We're only intended to be called if the server did send a partial
 * response.
 *
*/
static bool range_contains_eof(request_rec *r) {
	// Re-fetch the header. We can't use r->range because the byterange
	// filter messes that up.
	const char *range = apr_table_get(r->headers_in, "Range");
	if (!range)
		return false;
	
	// is this a bytes range?
	if (0 != strncasecmp(range, "bytes=", strlen("bytes="))) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
		              "Non-bytes range scheme in %s", range);
		return false;
	}
	range += strlen("bytes=");

	long long highbyte = -1; // hey, let's be 64-bit ready
	while (*range) {
		// Is this of the form -123, which means last 123 bytes?
		if ('-' == *range)
			return true;

		long long a __attribute__((unused)), b;
		char *end;
		
		// find start of range
		a = strtoll(range, &end, 10);
		while (' ' == *end)
			++end;
		if ('-' != *end) {
			ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
			              "Range without - or not number in %s", range); //FIXME: log full header
			return false; // corrupt range
		}
		if (',' == end[1] || 0 == end[1])
			return true; // 123- means bytes 123 through end
		
		// find end of range
		range = end+1;
		b = strtoll(range, &end, 10);
		range = end;

		if (b > highbyte)
			highbyte = b;

		while (' ' == *end)
			++end;
		if (*end) {
			if (',' != *end) {
				ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
							  "Range seperated wrong in %s", range); // FIXME: log full header
				return false; // corrupted range
			}
			range = end + 1;
		} else
			range = end;
	}
	
	// check if we found a range header
	if (highbyte == -1) {
		ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
					  "Range: header empty?!");
		return false;
	}

	// finally, stat the file to check if this is the eof.
	struct stat statbuf;
	if (-1 == stat(r->canonical_filename, &statbuf)) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR,
		              APR_FROM_OS_ERROR(errno), r, "Could not stat %s",
		              r->canonical_filename);
		return false;
	}

	// And compare. Remember that highbyte is from the range, which is
	// zero-based. Size, of course, needs 0 to mean empty file so that's
	// where the +1 comes from.
	return (highbyte+1 == statbuf.st_size);
}

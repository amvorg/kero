#! /bin/sh
#
# Author: Anthony DeRobertis <amv@derobert.net>. 
#
# Based on Debian skeleton init script by Miquel van Smoorenburg and Ian
# Murdock.
#

set -e

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DESC="Pancake download server"
NAME=Pancake
DAEMON=/usr/local/sbin/$NAME
PIDFILE=/var/local/run/pancake/pancake.pid
SCRIPTNAME=/etc/init.d/pancake

# Gracefully exit if the package has been removed.
test -x $DAEMON || exit 0

#
#	Function that starts the daemon/service.
#
d_start() {
	start-stop-daemon --start --quiet --pidfile $PIDFILE \
		-u pancake -g pancake -c pancake -d /etc -b -m \
		--exec $DAEMON
}

#
#	Function that stops the daemon/service.
#
d_stop() {
	start-stop-daemon --stop --quiet --pidfile $PIDFILE \
		-u pancake -g pancake --name $NAME
	rm -f $PIDFILE
}


case "$1" in
  start)
	echo -n "Starting $DESC: $NAME"
	d_start
	echo "."
	;;
  stop)
	echo -n "Stopping $DESC: $NAME"
	d_stop
	echo "."
	;;
  restart|force-reload)
	#
	#	If the "reload" option is implemented, move the "force-reload"
	#	option to the "reload" entry above. If not, "force-reload" is
	#	just the same as "restart".
	#
	echo -n "Restarting $DESC: $NAME"
	d_stop
	sleep 1
	d_start
	echo "."
	;;
  *)
	# echo "Usage: $SCRIPTNAME {start|stop|restart|reload|force-reload}" >&2
	echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload}" >&2
	exit 1
	;;
esac

exit 0

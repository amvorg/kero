MODSAVAIL := /etc/apache2/mods-available

all:
	$(MAKE) -C c

clean:
	$(MAKE) -C c clean

install: install-local-dirs
	$(MAKE) -C c install
	[ -f "$(MODSAVAIL)/pancake.conf" ] || install -o root -g root -p conf/mod_pancake.conf "$(MODSAVAIL)/pancake.conf"
	[ -f "$(MODSAVAIL)/pancake.load" ] || install -o root -g root -p conf/mod_pancake.load "$(MODSAVAIL)/pancake.load"
	install -p perl/Pancake               -o root -g root         /usr/local/sbin/
	install -p perl/Butter                -o root -g root         /usr/local/sbin/
	install -p conf/local-kerosuite.conf  -o root -g root -m 0644 /usr/local/lib/tmpfiles.d/
	install -p conf/local-pancake.service -o root -g root -m 0644 /usr/local/lib/systemd/system/
	install -p conf/local-butter.service  -o root -g root -m 0644 /usr/local/lib/systemd/system/
	[ -f /etc/local/pancake.conf ] || install -o root -g pancake -m 0640 conf/pancake.conf /etc/local/
	[ -f /etc/local/butter.conf ]  || install -o root -g root    -m 0600 conf/butter.conf  /etc/local/

install-local-dirs:
	[ -d /usr/local/lib/systemd ]        || install -d -o root -g root -m 0755 /usr/local/lib/systemd
	[ -d /usr/local/lib/systemd/system ] || install -d -o root -g root -m 0755 /usr/local/lib/systemd/system
	[ -d /usr/local/lib/tmpfiles.d ]     || install -d -o root -g root -m 0755 /usr/local/lib/tmpfiles.d
	[ -d /etc/local ]                    || install -d -o root -g root -m 0755 /etc/local

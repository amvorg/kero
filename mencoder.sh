#!/bin/sh
set -e

#echo -e '\n\n\n\n################################################################################\nDoing XVIDENC...'
#for pass in 1 2; do mencoder "$1" -o "$2.xvid.avi"  -endpos 30 -ffourcc DIVX -vf scale=208:160,pp=ha:c/va:c/dr:c,hqdn3d -channels 1 -srate 22050 -af pan=2:.5:.5:.5:.5,resample=22050 -of avi -ovc xvid -oac mp3lame -xvidencopts pass=$pass:bitrate=150:me_quality=6:max_key_interval=300:nopacked:max_bframes=2:trellis:nogmc:noqpel:vhq=1:overflow_control_strength=20:psnr:autoaspect -lameopts cbr:preset=32 ; done


echo -e '\n\n\n\n################################################################################\n################################################################################\n################################################################################\nDoing XVIDENC alternate (100 mq15)...'
for pass in 1 2; do mencoder "$1" -o "$2.xvid-alt100-mq15.avi"  -endpos 30 -ffourcc DIVX -vf scale=208:160,pp=ha:c/va:c/dr:c,hqdn3d -channels 1 -srate 22050 -af pan=2:.5:.5:.5:.5,resample=22050 -of avi -ovc xvid -oac mp3lame -xvidencopts pass=$pass:bitrate=100:me_quality=6:max_key_interval=300:nopacked:max_bframes=2:trellis:gmc:chroma_opt:hq_ac:noqpel:vhq=4:autoaspect:overflow_control_strength=20:max_iquant=15:max_pquant=15:max_bquant=15:psnr -lameopts cbr:preset=32 ; done


###   echo -e '\n\n\n\n################################################################################\n################################################################################\n################################################################################\nDoing XVIDENC alternate (50)...'
###   for pass in 1 2; do mencoder "$1" -o "$2.xvid-alt050.avi"  -endpos 30 -ffourcc DIVX -vf scale=208:160,pp=ha:c/va:c/dr:c,hqdn3d -channels 1 -srate 22050 -af pan=2:.5:.5:.5:.5,resample=22050 -of avi -ovc xvid -oac mp3lame -xvidencopts pass=$pass:bitrate=50:me_quality=6:max_key_interval=300:nopacked:max_bframes=2:trellis:gmc:chroma_opt:hq_ac:noqpel:vhq=4:autoaspect:overflow_control_strength=20:psnr -lameopts cbr:preset=32 ; done
###   
###   
###   echo -e '\n\n\n\n################################################################################\n################################################################################\n################################################################################\nDoing XVIDENC alternate (100)...'
###   for pass in 1 2; do mencoder "$1" -o "$2.xvid-alt100.avi"  -endpos 30 -ffourcc DIVX -vf scale=208:160,pp=ha:c/va:c/dr:c,hqdn3d -channels 1 -srate 22050 -af pan=2:.5:.5:.5:.5,resample=22050 -of avi -ovc xvid -oac mp3lame -xvidencopts pass=$pass:bitrate=100:me_quality=6:max_key_interval=300:nopacked:max_bframes=2:trellis:gmc:chroma_opt:hq_ac:noqpel:vhq=4:autoaspect:overflow_control_strength=20:psnr -lameopts cbr:preset=32 ; done
###   
###   
###   echo -e '\n\n\n\n################################################################################\n################################################################################\n################################################################################\nDoing XVIDENC alternate (150)...'
###   for pass in 1 2; do mencoder "$1" -o "$2.xvid-alt150.avi"  -endpos 30 -ffourcc DIVX -vf scale=208:160,pp=ha:c/va:c/dr:c,hqdn3d -channels 1 -srate 22050 -af pan=2:.5:.5:.5:.5,resample=22050 -of avi -ovc xvid -oac mp3lame -xvidencopts pass=$pass:bitrate=150:me_quality=6:max_key_interval=300:nopacked:max_bframes=2:trellis:gmc:chroma_opt:hq_ac:noqpel:vhq=4:autoaspect:overflow_control_strength=20:psnr -lameopts cbr:preset=32 ; done
###   
###   
###   echo -e '\n\n\n\n################################################################################\n################################################################################\n################################################################################\nDoing XVIDENC alternate (200)...'
###   for pass in 1 2; do mencoder "$1" -o "$2.xvid-alt200.avi"  -endpos 30 -ffourcc DIVX -vf scale=208:160,pp=ha:c/va:c/dr:c,hqdn3d -channels 1 -srate 22050 -af pan=2:.5:.5:.5:.5,resample=22050 -of avi -ovc xvid -oac mp3lame -xvidencopts pass=$pass:bitrate=200:me_quality=6:max_key_interval=300:nopacked:max_bframes=2:trellis:gmc:chroma_opt:hq_ac:noqpel:vhq=4:autoaspect:overflow_control_strength=20:psnr -lameopts cbr:preset=32 ; done
###   

echo -e '\n\n\n\n################################################################################\n################################################################################\n################################################################################\nDoing LAVC...'
for pass in 1 2; do mencoder "$1" -o "$2.lavc.avi"  -endpos 30 -ffourcc DIVX -vf scale=208:160,pp=ha:c/va:c/dr:c,hqdn3d -channels 1 -srate 22050 -af pan=2:.5:.5:.5:.5,resample=22050 -of avi -ovc lavc -oac mp3lame -lavcopts acodec=mp3:abitrate=32:vcodec=mpeg4:vmax_b_frames=2:preme=2:mbd=1:v4mv:keyint=300:autoaspect:vbitrate=150:dia=2:trell:sc_threshold=1000000:vpass=$pass:psnr -lameopts cbr:preset=32; done


This is the code that handles videos on the video server. It includes
an Apache module (mod_pancake) and related daemon (Panckae) to record
downloads in the database and a daemon (Butter) to handle confirming
videos.

Status: In production for years.

Todo: Test suite is partially created. Needs a bunch of work to make it
worthwhile.
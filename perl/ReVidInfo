#!/usr/bin/perl -Tw

use DBI;
use Carp;
use Digest::MD5;
use POSIX qw(setuid setgid);
use Unix::Syslog qw(:macros :subs);
use Config::Simple qw(-lc);
use strict;

### Prototypes
sub get_file_info($);
sub safe_command_capture(@);
sub do_unprivileged(&);
sub load_config();
sub get_dbconn();

### Gloabal variables.
our %gConfig;         # used (r/o) all over the place.

my %EXT_INFO_PROG = (
	mpg  => 'mpeg',
	mpeg => 'mpeg',
	ogm  => 'ogm',
	avi  => 'mplayer',
	wmv  => 'mplayer',
	asf  => 'mplayer',
	mp4  => 'mplayer',
	mkv  => 'mplayer',
	mov  => 'mplayer',
	flv  => 'mplayer',
);

redo_missing_vidinfo();

sub redo_missing_vidinfo {
	load_config();

	my $vids
		= get_dbconn()->selectcol_arrayref(<<QUERY, {}, $gConfig{server_id});
SELECT vid_id FROM videos WHERE
      local_download_server = ?
  AND local = 'TRUE'
  AND local_hidden = 'FALSE'
  AND is_deleted = 'FALSE'
  AND vid_info = ''
QUERY
	
	foreach my $vid (@$vids) {
		redo_one_vidinfo($vid);
	}
}

sub redo_one_vidinfo {
	my ($vid_id) = @_;
	my $dbh = get_dbconn();
	my $sth;

	$sth = $dbh->prepare_cached(<<QUERY);
SELECT user_id, local_filename FROM videos WHERE vid_id = ?
QUERY
	$sth->execute($vid_id);
	my ($user_id, $name) = $sth->fetchrow_array
		or confess "Could not find vid_id $vid_id";
	$sth->finish;

	my $path = sprintf('%s/%i/%02i/%i/%i/%s', $gConfig{confirmed},
		$user_id%10, $user_id%100, $user_id, $vid_id, $name);

	print STDERR "Doing $vid_id... ";

	if (! -f $path ) {
		warn "Could not find $path";
		return;
	}

	my ($type, $info) = eval { get_file_info($path) };
	if ($@) {
		warn "ERROR: $@";
		return;
	}

	$sth = $dbh->prepare_cached(<<QUERY);
UPDATE videos SET vid_info_type = ?, vid_info = ? WHERE vid_id = ?
QUERY
	$sth->execute($type, $info, $vid_id);
	$sth->finish;

	print STDERR "done.\n";
}

sub get_file_info($) {
	my $file = $_[0];

	my $ext;
	if (!($file =~ /\.([^\/.]+)$/)) {
		return undef;
	} else {
		$ext = lc($1);
	}

	exists $EXT_INFO_PROG{$ext}
		or return undef, undef;

	if ('mpeg' eq $EXT_INFO_PROG{$ext}) {
		return MPG => safe_command_capture('mpginfo', $file);
	} elsif ('ogm' eq $EXT_INFO_PROG{$ext}) {
		return OGM => safe_command_capture('ogminfo', '-v', $file);
	} elsif ('mplayer' eq $EXT_INFO_PROG{$ext}) {
		my @tmp = split('\n', safe_command_capture(
			'mplayer', '-identify', '-ao', 'null', '-vo', 'null',
			           '-frames', '0', '-quiet', $file
		));
		my $res;
		foreach my $line (@tmp) {
			$line =~ /^ID_/
				and $res .= "$line\n";
		}
		return uc($ext), $res;
	} else {
		die "Uknown \$EXT_INFO_PROG{$ext} = $EXT_INFO_PROG{$ext}";
	}
}


sub safe_command_capture(@) {
	my $command = shift;
	my @args = @_;

	return do_unprivileged {
		$ENV{PATH} = '/bin:/usr/bin';
		exec { $command } $command, @args or confess "exec: $!";
	}
}


sub do_unprivileged(&) {
	my $coderef = $_[0];

	# Find the UID/GID to switch to
	my $nogroup = getgrnam('nogroup')
		or confess "getgrnam('nogroup'): $!";
	my $nobody = getpwnam('nobody')
		or confess "getpwnam('nobody'): $!";
	
	# Pipe to ourselves. Easy perl way of doing the whole pipe/fork mess
	# you'd have to do in C.
	my $pipe;
	defined(my $pid = open($pipe, '-|')) or confess "open: $!";
	
	# If this is the child process, execute the coderef and exit.
	if (0 == $pid) {
		close STDERR or confess "close STDERR: $!";
		open STDERR, '>&STDOUT'; # dup
		POSIX::setgid($nogroup) or confess "setgid: $!";
		POSIX::setuid($nobody) or confess "setuid: $!";
		eval { &$coderef() };
		if ($@) {
			my $err = $@;
			syslog(LOG_ERR, "do_unprivileged: coderef gave error: $err");
			print $err, "\n";
		}
		exit;
	}

	# The child process exited above, so this is the parent. Read what
	# the child has to say.
	my ($buffer, $read);
	$buffer .= $read while (defined($read = <$pipe>));

	# Child has no more to say, try close. Close will fail if the child
	# exited uncleanly. If so, turn the unclean exit into an exception.
	close($pipe) or die "$buffer (\$?=$?)";

	# Exit was clean, return whatever the child said.
	return $buffer;
}

sub load_config() {
	our %gConfig;

	# Do the load.
	Config::Simple->import_from('butter.conf', \%gConfig)
		or croak (Config::Simple->error());

	# Now, for the real point of this function: Check its validity.
	# Well, somewhat at least.
	my $bad = 0;
	exists $gConfig{confirmed}
		or $bad = 1, print STDERR "CONFIRMED not defined in config.\n";
	exists $gConfig{incoming}
		or $bad = 1, print STDERR "INCOMING not defined in config.\n";
	exists $gConfig{dsn}
		or $bad = 1, print STDERR "DSN not defined in config.\n";
	exists $gConfig{user}
		or $bad = 1, print STDERR "USER not defined in config.\n";
	exists $gConfig{pass}
		or $bad = 1, print STDERR "PASS not defined in config.\n";
	exists $gConfig{test_mode} && ($gConfig{test_mode} == 0 ||
	                               $gConfig{test_mode} == 1)
		or $bad = 1, print STDERR "TEST_MODE wrong/missing in config.\n";

	if ($bad) {
		print STDERR "Configuration file contains errors. Bailing out.\n";
		exit 1;
	}
}

sub get_dbconn() {
	our %gConfig;
	return DBI->connect_cached($gConfig{dsn}, $gConfig{user},
	                           $gConfig{pass});
}
